import React from 'react'
import { Layout, PageBlock } from 'vtex.styleguide'

const AdminExample = () => {
    return (
        <Layout>
            <PageBlock
                title="Training Colombia"
                subtitle="Admin Example"
                variation="full"
            >
                <div>
                    Vtex Colombia
                </div>
            </PageBlock>
        </Layout>
    )
}

export default AdminExample
