import type { FC } from 'react'
import { injectIntl } from 'react-intl'

interface RenderProps{
    name: string
    path: string
}

interface Props {
    render: (paths: RenderProps[]) => any
    intl: any
}

const MyAccountExampleLink:FC<Props> = ({render, intl}: Props) => {
    return render([
        {
            name: intl.formatMessage({id: 'store/my-account-link'}),
            path: '/myaccountexample'
        }
    ])
}

export default injectIntl(MyAccountExampleLink)
