import React from 'react'
import { Route } from 'react-router-dom'
import VtexColombia from './components/VtexColombia/VtexColombia'

const MyAccountExamplePage = () => {
    return (
        <div>
            <Route
                path="/myaccountexample"
                exact
                component={() => <VtexColombia />}
            ></Route>
            
        </div>
    )
}

export default MyAccountExamplePage
